def guessing_game
  number = rand(1..100)

  guesses_needed = 0
  loop do
    puts "guess a number"
    guess = gets.chomp.to_i
    guesses_needed += 1

    if guess < number
       puts "#{guess} is too low"
    elsif guess == number
        puts "You found the number: #{guess}"
        break
    else
        puts "#{guess} is too high"
    end
  end
  puts "It took you #{guesses_needed} guesses"
end

def file_shuffler
  puts "give me a file name"
  file_name = gets.chomp
  arr = []
  File.open(file_name).each { |line| arr << line }
  arr.shuffle!
  arr.each do |line|
    File.open(file_name, "a")
    print line
  end

end
